// $(document).ready(function(){
//     $("#InProcess").click(function(){
//         $(".col-md-6").hide();
//     });
//     $("#Sold").click(function(){
//         $(".row").show();
//     });
// });
var discount = "(Discount) ";
$(document).ready(function() {
  var sessionID = sessionStorage.getItem("userSessionID", sessionID);
  $("#selectTag").val("1");
  $(".Sold").hide();
  $("#InProcess2").show();
  $("#Sold2").show();
  $("#Sold2").removeAttr("style");

  $("#dateButton").click(function() {
    $(".dates").show();
    $(".selectdate2").show();
    $(".selectdate1").hide();
  });

  $("#selectTag").click(function() {
    $("#selectTag").val("1");
    $("#InProcess").show();
    $("#Sold").show();
    $("#InProcess2").show();
    $("#Sold2").show();
  });

  $("#selectTag2").click(function() {
    $("#InProcess").show();
    $("#Sold").show();
    $("#InProcess2").show();
    $("#Sold2").show();
  });

  $("#dateButton2").click(function() {
    $(".dates").hide();
    $(".selectdate2").hide();
    $(".selectdate1").show();
  });

  $("#InProcess").click(function() {
    $("#selectTag").val("1");
    $("#selectTag2").val("1");
    $(".Sold").hide();
    $(".InProcess").show();
    $("#InProcess").show();
    $("#Sold").show();
    $("#Sold2").removeAttr("style");
  });

  $("#Sold").click(function() {
    $("#selectTag").val("2");
    $("#selectTag2").val("2");
    $("#Sold").show();
    $("#InProcess").show();
    $(".InProcess").hide();
    $(".Sold").show();
    $(".selectdate2").show();
    $(".dates").show();
    $(".selectdate1").hide();
  });

  $("#InProcess2").click(function() {
    $("#selectTag").val("1");
    $("#selectTag2").val("1");
    $("#InProcess2").show();
    $("#Sold").show();
    $(".Sold").hide();
    $(".InProcess").show();
  });

  $(".closemodal").click(function() {
    $('#SubTotal').text('');
    $('#Total').text('');
    $("#AppliedAmount").text('');
    $("#TenderTypeToken").text('');
    discount = '';
  });

  $("#Sold2").click(function() {
    $("#selectTag2").val("2");
    $("#selectTag").val("2");
    $("#Sold2").show();
    $("#InProcess2").show();
    $(".InProcess").hide();
    $(".Sold").show();
    $(".selectdate2").show();
    $(".dates").show();
    $(".selectdate1").hide();
    $("#InProcess").removeAttr("style");

    var totalAmountToBePaid;
    

    //Gets the involist list to the view order table
    if ($("#invoice3").val() == null) {
      var d = new Date();
      var month = d.getMonth() + 1;
      var day = d.getDate();
      var output =
        d.getFullYear() +
        "/" +
        (month < 10 ? "0" : "") +
        month +
        "/" +
        (day < 10 ? "0" : "") +
        day;
      $.post(
        "https://api.mydrycleaner.com/q",
        JSON.stringify({
          RequestType: "InvoicesList",
          AccountKey: "NGPERI1344",
          SessionID: sessionID,
          Parameters: {
            FilterTypeID: 128,
            StartDate: "01/01/2018",
            EndDate: output
          }
        }),
        function(data, status) {
          var InvoicesList = JSON.parse(data);
          var returnObject = InvoicesList.ReturnObject;
          console.log(returnObject);
          sessionStorage.setItem(
            "invoiceID",
            InvoicesList.ReturnObject.InvoiceID
          );
          //on success appends the return json to the <td> tags
          for (var i = 0; i < returnObject.length; i++) {
            //on success append to <option id= sold> <td> table
            if (returnObject.SoldDateTime != "0001-01-01T00:00:00") {
              var date3 = new Date(returnObject[i].SoldDateTime);
              var month3 = date3.getMonth() + 1;
              var day3 = date3.getDate();
              var output3 =
                date3.getFullYear() +
                "/" +
                (month3 < 10 ? "0" : "") +
                month3 +
                "/" +
                (day3 < 10 ? "0" : "") +
                day3;
              $("#NotFound").hide();
              $("#example1").append(
                "<tr><td>" +
                  output3 +
                  '</td> <td><a id="invoice3" href="#"  role="button" data-toggle="modal" data-target="#login-modal">' +
                  returnObject[i].InvoiceKey +
                  "</a></td> <td>" +
                  returnObject[i].DepartmentGroupName +
                  " <td> " +
                  returnObject[i].Balance +
                  " </td><tr>"
              );
            } else {
              $("#NotFound").show();
            }
          }
        }
      ).fail(function(error) {
        notif({
          msg: "Session expired! please login",
          type: "error",
          timeout: 3000
        });
        window.location.href = "../../index.html";
        console.log(error);
      });

      //on click of sold table modal
      $(document).on("click", "#invoice3", function() {
        $("#div-forms i").addClass("fa fa-spinner fa-spin");
        var invoicekey = $(this).text();
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var output =
          d.getFullYear() +
          "/" +
          (month < 10 ? "0" : "") +
          month +
          "/" +
          (day < 10 ? "0" : "") +
          day;
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "InvoicesList",
            AccountKey: "NGPERI1344",
            SessionID: sessionID,
            Parameters: {
              FilterTypeID: 128,
              StartDate: "10/22/2000",
              EndDate: output
            }
          }),
          function(data, status) {
            var InvoicesList = JSON.parse(data);
            var returnObject = InvoicesList.ReturnObject;
            console.log(InvoicesList);
            //gets the invoiceID for the click invoice of the table
            var returnedData = $.grep(returnObject, function(element, index) {
              return element.InvoiceKey == invoicekey;
            });
            //console.log(returnedData[0].InvoiceID);
            //gets the invoice detail by invoiceID
            $.post(
              "https://api.mydrycleaner.com/q",
              JSON.stringify({
                RequestType: "InvoiceDetail",
                AccountKey: "NGPERI1344",
                SessionID: sessionID,
                Parameters: {
                  InvoiceID: returnedData[0].InvoiceID
                }
              }),
              function(data, status) {
                var InvoicesDetail = JSON.parse(data);
                $("#div-forms i").removeClass("fa fa-spinner fa-spin");
                console.log("1");
                console.log(InvoicesDetail.ReturnObject.Items);
                var date = new Date(InvoicesDetail.ReturnObject.PromisedDate);
                var month = date.getMonth() + 1;
                var day = date.getDate();
                var output =
                  date.getFullYear() +
                  "/" +
                  (month < 10 ? "0" : "") +
                  month +
                  "/" +
                  (day < 10 ? "0" : "") +
                  day;
                //on success appends to the pop out <p> tags

                $("#DcRegular").append(
                  InvoicesDetail.ReturnObject.Items[0].DepartmentName
                );
                $("#keyinvoice").append(InvoicesDetail.ReturnObject.InvoiceKey);

                invoiceKey = InvoicesDetail.ReturnObject.InvoiceKey;
                $("#promiseddate").append("Promised:  " + output);
                $.each(InvoicesDetail.ReturnObject.Items, function(key, val) {
                  if (val.Details[0] == null) {
                    $("#orderdetails").append(
                      '<p style="text-align:left;">' +
                        val.CategoryName +
                        "- " +
                        val.ItemName +
                        '<span id="Amount" style="float:right;"><b>' +
                        val.Amount +
                        ' </b></span> </p>   <p id="Name">' +
                        "No color name" +
                        " </p>"
                    );
                  } else {
                    $("#orderdetails").append(
                      '<p style="text-align:left;">' +
                        val.CategoryName +
                        "- " +
                        val.ItemName +
                        '<span id="Amount" style="float:right;"><b>' +
                        val.Amount +
                        ' </b></span> </p>   <p id="Name">' +
                        val.Details[0].Name +
                        " </p>"
                    );
                  }
                  // $("#orderdetails").append(
                  //   '<p style="text-align:left;">' +
                  //     val.CategoryName +
                  //     "- " +
                  //     val.ItemName +
                  //     '<span id="Amount" style="float:right;"><b>' +
                  //     val.Amount +
                  //     ' </b></span> </p>   <p id="Name">' +
                  //     val.Details[0].Name +
                  //     " </p>"
                  // );
                });
                //calculates the balance of invoice detail amounts
                var Amount = InvoicesDetail.ReturnObject.Items.length;
                function sum(input) {
                  var total = 0;
                  for (var i = 0; i < Amount; i++) {
                    if (isNaN(input[i].Amount)) {
                      continue;
                    }
                    total += Number(input[i].Amount);
                    // console.log(total);
                  }
                  return total;
                }
                // appends to <b> balance tag after it calculate all amounts

                $("#Balance").append("N0.00");
                console.log(sum(InvoicesDetail.ReturnObject.Items));
              }
            ).fail(function(error) {
              notif({
                msg: "Session expired! please login",
                type: "error",
                timeout: 3000
              });
              window.location.href = "../../index.html";
              console.log(error);
              $("#div-forms i").removeClass("fa fa-spinner fa-spin");
            });
          }
        ).fail(function(error) {
          notif({
            msg: "Session expired! please login",
            type: "error",
            timeout: 3000
          });
          $("#div-forms i").removeClass("fa fa-spinner fa-spin");
          window.location.href = "../../index.html";
          console.log(error);
        });
      });
    }
  });

  //on modal close, empty some things,
  $("#login-modal").on("show.bs.modal", function() {
    $("#DcRegular").empty();
    $("#keyinvoice").empty();
    $("#promiseddate").empty();
    $("#orderdetails").empty();
    $("#Balance").empty();
    $("#paymentButton").prop("disabled", true);
  });
  // on click of date filter invoice list endpoint by date
  var startDate;
  $("#datepicker").on("change", function() {
    startDate = $(this).val();
  });
  $("#datepicker2").on("change", function() {
    var endDate = $(this).val();
    //Gets the involist list to the view order table
    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "InvoicesList",
        AccountKey: "NGPERI1344",
        SessionID: sessionID,
        Parameters: {
          FilterTypeID: 130,
          StartDate: startDate,
          EndDate: endDate
        }
      }),
      function(data, status) {
        var InvoicesList = JSON.parse(data);
        var returnObject = InvoicesList.ReturnObject;
        console.log(returnObject);
        sessionStorage.setItem(
          "invoiceID",
          InvoicesList.ReturnObject.InvoiceID
        );
        //on success appends the return json to the <td> tags
        for (var i = 0; i < returnObject.length; i++) {
          //on success append to <option id= sold> <td> table
          if (returnObject.SoldDateTime != "0001-01-01T00:00:00") {
            var date3 = new Date(returnObject[i].SoldDateTime);
            var month3 = date3.getMonth() + 1;
            var day3 = date3.getDate();
            var output3 =
              date3.getFullYear() +
              "/" +
              (month3 < 10 ? "0" : "") +
              month3 +
              "/" +
              (day3 < 10 ? "0" : "") +
              day3;
            $("#NotFound").hide();
            $("#example1").append(
              "<tr><td>" +
                output3 +
                '</td> <td><a id="invoice3" href="#"  role="button" data-toggle="modal" data-target="#login-modal">' +
                returnObject[i].InvoiceKey +
                "</a></td> <td>" +
                returnObject[i].DepartmentGroupName +
                " <td> " +
                returnObject[i].Balance +
                " </td><tr>"
            );
          } else {
            $("#NotFound").show();
          }
        }
      }
    ).fail(function(error) {
      notif({
        msg: "Session expired! please login",
        type: "error",
        timeout: 3000
      });
      window.location.href = "../../index.html";
      console.log(error);
    });

    //on click of sold table modal
    $(document).on("click", "#invoice3", function() {
      $("#div-forms i").addClass("fa fa-spinner fa-spin");
      var invoicekey = $(this).text();
      var d = new Date();
      var month = d.getMonth() + 1;
      var day = d.getDate();
      var output =
        d.getFullYear() +
        "/" +
        (month < 10 ? "0" : "") +
        month +
        "/" +
        (day < 10 ? "0" : "") +
        day;
      $.post(
        "https://api.mydrycleaner.com/q",
        JSON.stringify({
          RequestType: "InvoicesList",
          AccountKey: "NGPERI1344",
          SessionID: sessionID,
          Parameters: {
            FilterTypeID: 130,
            StartDate: "10/22/2018",
            EndDate: output
          }
        }),
        function(data, status) {
          var InvoicesList = JSON.parse(data);
          var returnObject = InvoicesList.ReturnObject;
          //gets the invoiceID for the click invoice of the table
          var returnedData = $.grep(returnObject, function(element, index) {
            return element.InvoiceKey == invoicekey;
          });
          console.log(returnedData[0].InvoiceID);
          //gets the invoice detail by invoiceID
          $.post(
            "https://api.mydrycleaner.com/q",
            JSON.stringify({
              RequestType: "InvoiceDetail",
              AccountKey: "NGPERI1344",
              SessionID: sessionID,
              Parameters: {
                InvoiceID: returnedData[0].InvoiceID
              }
            }),
            function(data, status) {
              var InvoicesDetail = JSON.parse(data);
              $("#div-forms i").removeClass("fa fa-spinner fa-spin");
              console.log(InvoicesDetail.ReturnObject.Items);
              var date = new Date(InvoicesDetail.ReturnObject.PromisedDate);
              var month = date.getMonth() + 1;
              var day = date.getDate();
              var output =
                date.getFullYear() +
                "/" +
                (month < 10 ? "0" : "") +
                month +
                "/" +
                (day < 10 ? "0" : "") +
                day;
              //on success appends to the pop out <p> tags

              $("#DcRegular").append(
                InvoicesDetail.ReturnObject.Items[0].DepartmentName
              );
              $("#keyinvoice").append(InvoicesDetail.ReturnObject.InvoiceKey);

              invoiceKey = InvoicesDetail.ReturnObject.InvoiceKey;
              $("#promiseddate").append("Promised:  " + output);
              $.each(InvoicesDetail.ReturnObject.Items, function(key, val) {
                if (val.Details[0] == null) {
                  $("#orderdetails").append(
                    '<p style="text-align:left;">' +
                      val.CategoryName +
                      "- " +
                      val.ItemName +
                      '<span id="Amount" style="float:right;"><b>' +
                      val.Amount +
                      ' </b></span> </p>   <p id="Name">' +
                      "No color name" +
                      " </p>"
                  );
                } else {
                  $("#orderdetails").append(
                    '<p style="text-align:left;">' +
                      val.CategoryName +
                      "- " +
                      val.ItemName +
                      '<span id="Amount" style="float:right;"><b>' +
                      val.Amount +
                      ' </b></span> </p>   <p id="Name">' +
                      val.Details[0].Name +
                      " </p>"
                  );
                }
              }),
                function(data, status) {
                  var InvoicesDetail = JSON.parse(data);
                  $("#div-forms i").removeClass("fa fa-spinner fa-spin");
                  console.log(InvoicesDetail.ReturnObject.Items);
                  var date = new Date(InvoicesDetail.ReturnObject.PromisedDate);
                  var month = date.getMonth() + 1;
                  var day = date.getDate();
                  var output =
                    date.getFullYear() +
                    "/" +
                    (month < 10 ? "0" : "") +
                    month +
                    "/" +
                    (day < 10 ? "0" : "") +
                    day;
                  //on success appends to the pop out <p> tags

                  $("#DcRegular").append(
                    InvoicesDetail.ReturnObject.Items[0].DepartmentName
                  );
                  $("#keyinvoice").append(
                    InvoicesDetail.ReturnObject.InvoiceKey
                  );

                  invoiceKey = InvoicesDetail.ReturnObject.InvoiceKey;
                  $("#promiseddate").append("Promised:  " + output);
                  $.each(InvoicesDetail.ReturnObject.Items, function(key, val) {
                    if (val.Details[0] == null) {
                      $("#orderdetails").append(
                        '<p style="text-align:left;">' +
                          val.CategoryName +
                          "- " +
                          val.ItemName +
                          '<span id="Amount" style="float:right;"><b>' +
                          val.Amount +
                          ' </b></span> </p>   <p id="Name">' +
                          "No color name" +
                          " </p>"
                      );
                    } else {
                      $("#orderdetails").append(
                        '<p style="text-align:left;">' +
                          val.CategoryName +
                          "- " +
                          val.ItemName +
                          '<span id="Amount" style="float:right;"><b>' +
                          val.Amount +
                          ' </b></span> </p>   <p id="Name">' +
                          val.Details[0].Name +
                          " </p>"
                      );
                    }
                  });
                  //calculates the balance of invoice detail amounts
                  var Amount = InvoicesDetail.ReturnObject.Items.length;
                  function sum(input) {
                    var total = 0;
                    for (var i = 0; i < Amount; i++) {
                      if (isNaN(input[i].Amount)) {
                        continue;
                      }
                      total += Number(input[i].Amount);
                      // console.log(total);
                    }
                    total += Number(input[i].Amount);
                    // console.log(total);
                  }
                  return total;
                };
              // appends to <b> balance tag after it calculate all amounts

              $("#Balance").append("N0.00");
              console.log(sum(InvoicesDetail.ReturnObject.Items));
            }
          ).fail(function(error) {
            notif({
              msg: "Session expired! please login",
              type: "error",
              timeout: 3000
            });
            window.location.href = "../../index.html";
            console.log(error);
            $("#div-forms i").removeClass("fa fa-spinner fa-spin");
          });
        }
      ).fail(function(error) {
        notif({
          msg: "Session expired! please login",
          type: "error",
          timeout: 3000
        });
        $("#div-forms i").removeClass("fa fa-spinner fa-spin");
        window.location.href = "../../index.html";
        console.log(error);
      });
    });
  });

  var todaysDate = new Date();
  var month4 = todaysDate.getMonth() + 1;
  var day4 = todaysDate.getDate();
  var output4 =
    todaysDate.getFullYear() +
    "/" +
    (month4 < 10 ? "0" : "") +
    month4 +
    "/" +
    (day4 < 10 ? "0" : "") +
    day4;

  //Gets the involist list to the view order table
  $.post(
    "https://api.mydrycleaner.com/q",
    JSON.stringify({
      RequestType: "InvoicesList",
      AccountKey: "NGPERI1344",
      SessionID: sessionID,
      Parameters: {
        FilterTypeID: 128,
        StartDate: "10/22/2018",
        EndDate: output4
      }
    }),
    function(data, status) {
      var InvoicesList = JSON.parse(data);
      var returnObject = InvoicesList.ReturnObject;
      console.log(returnObject);
      sessionStorage.setItem("invoiceID", InvoicesList.ReturnObject.InvoiceID);
      $("#processdate").empty();
      $("#readydate").empty();
      $("#invoice2").empty();
      $("#balance2").empty();
      $("#SoldDateTime").empty();
      $("#invoice3").empty();
      $("#balance3").empty();
      //on success appends the return json to the <td> tags
      //  $.each(returnObject, function(key,val){
      for (var i = 0; i < returnObject.length; i++) {
        var date = new Date(returnObject[i].PromisedDate);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var output =
          date.getFullYear() +
          "/" +
          (month < 10 ? "0" : "") +
          month +
          "/" +
          (day < 10 ? "0" : "") +
          day;
        if (output == null) {
          $("#NotFound4").show();
        }
        //    $("#processdate").append(output);
        //  $("#invoice").append(val.InvoiceKey);
        //  $("#department").append(val.DepartmentGroupName);
        $("#NotFound4").hide();
        $("#example2").append(
          "<tr><td>" +
            output +
            '</td> <td  id="invoice"><a href="#"  role="button" data-toggle="modal" data-target="#login-modal">' +
            returnObject[i].InvoiceKey +
            "</a></td> <td>" +
            returnObject[i].DepartmentGroupName +
            " <td> " +
            returnObject[i].Balance +
            " </td><tr>"
        );
        // //on success appends the return json to the second <td> tags
        if (returnObject[i].ReadyDateTime != "0001-01-01T00:00:00") {
          $("#NotFound2").show();
          var date2 = new Date(returnObject[i].ReadyDateTime);
          var month2 = date2.getMonth() + 1;
          var day2 = date2.getDate();
          var output2 =
            date2.getFullYear() +
            "/" +
            (month2 < 10 ? "0" : "") +
            month2 +
            "/" +
            (day2 < 10 ? "0" : "") +
            day2;

          $("#readydate").append(output2);
          $("#invoice2").append(returnObject[i].InvoiceKey);
          $("#balance2").append(returnObject[i].Balance);
        } else {
          $("#NotFound2").show();
        }
        //on success append to <option id= sold> <td> table
        if (returnObject[i].SoldDateTime != "0001-01-01T00:00:00") {
          var date3 = new Date(returnObject[i].SoldDateTime);
          var month3 = date3.getMonth() + 1;
          var day3 = date3.getDate();
          var output3 =
            date3.getFullYear() +
            "/" +
            (month3 < 10 ? "0" : "") +
            month3 +
            "/" +
            (day < 10 ? "0" : "") +
            day3;
          $("#NotFound").hide();
          $("#SoldDateTime").append(output3);
          $("#invoice3").append(returnObject[i].InvoiceKey);
          $("#balance3").append(returnObject[i].Balance);
        } else {
          $("#NotFound").show();
        }
      }
      //  })
    }
  ).fail(function(error) {
    notif({
      msg: "Session expired! please login",
      type: "error",
      timeout: 3000
    });
    window.location.href = "../../index.html";
    console.log(error);
  });
  //on click of invoice column gets  the invoice list
  $(document).on("click", "#invoice", function() {
    $("#div-forms i").addClass("fa fa-spinner fa-spin");
    var invoicekey = $(this).text();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output =
      d.getFullYear() +
      "/" +
      (month < 10 ? "0" : "") +
      month +
      "/" +
      (day < 10 ? "0" : "") +
      day;
    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "InvoicesList",
        AccountKey: "NGPERI1344",
        SessionID: sessionID,
        Parameters: {
          FilterTypeID: 128,
          StartDate: "10/22/2018",
          EndDate: output
        }
      }),
      function(data, status) {
        var InvoicesList = JSON.parse(data);
        var returnObject = InvoicesList.ReturnObject;
        //gets the invoiceID for the click invoice of the table
        var returnedData = $.grep(returnObject, function(element, index) {
          return element.InvoiceKey == invoicekey;
        });
        console.log(returnedData[0].InvoiceID);
        //gets the invoice detail by invoiceID
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "InvoiceDetail",
            AccountKey: "NGPERI1344",
            SessionID: sessionID,
            Parameters: {
              InvoiceID: returnedData[0].InvoiceID
            }
          }),
          function(data, status) {
            var InvoicesDetail = JSON.parse(data);
            $("#div-forms i").removeClass("fa fa-spinner fa-spin");
            $("#paymentButton").prop("disabled", false);
            totalAmountToBePaid = InvoicesDetail.ReturnObject.Total;
            console.log(InvoicesDetail.ReturnObject.Total);
            console.log(InvoicesDetail.ReturnObject.Items);
            var date = new Date(InvoicesDetail.ReturnObject.PromisedDate);
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var output =
              date.getFullYear() +
              "/" +
              (month < 10 ? "0" : "") +
              month +
              "/" +
              (day < 10 ? "0" : "") +
              day;
            //on success appends to the pop out <p> tags

            $("#DcRegular").append(
              InvoicesDetail.ReturnObject.Items[0].DepartmentName
            );
            $("#keyinvoice").append(InvoicesDetail.ReturnObject.InvoiceKey);
            invoiceKey = InvoicesDetail.ReturnObject.InvoiceKey;

            $("#promiseddate").append("Promised:  " + output);
            $.each(InvoicesDetail.ReturnObject.Items, function(key, val) {
              if (val.Details[0] == null) {
                $("#orderdetails").append(
                  '<p style="text-align:left;">' +
                    val.Quantity + ' - ' + val.CategoryName +
                    "- " +
                    val.ItemName +
                    '<span id="Amount" style="float:right;"><b>' +
                    val.Amount +
                    ' </b></span> </p>   <p id="Name">' +
                    "No color name" +
                    " </p>"
                );
              } else {
                $("#orderdetails").append(
                  '<p style="text-align:left;">' +
                  val.Quantity + ' - ' + val.CategoryName +
                    "- " +
                    val.ItemName +
                    '<span id="Amount" style="float:right;"><b>' +
                    val.Amount +
                    ' </b></span> </p>   <p id="Name">' +
                    val.Details[0].Name +
                    " </p>"
                );
              }
              // $("#orderdetails").append(
              //   '<p style="text-align:left;">' +
              //     val.CategoryName +
              //     "- " +
              //     val.ItemName +
              //     '<span id="Amount" style="float:right;"><b>' +
              //     val.Amount +
              //     ' </b></span> </p>   <p id="Name">' +
              //     val.Details[0].Name +
              //     " </p>"
              // );
            });
            //calculates the balance of invoice detail amounts
            var Amount = InvoicesDetail.ReturnObject.Items.length;
            function sum(input) {
              var total = 0;
              for (var i = 0; i < Amount; i++) {
                if (isNaN(input[i].Amount)) {
                  continue;
                }
                total += Number(input[i].Amount);
                // console.log(total);
              }
              return total;
            }
            // appends to <b> balance tag after it calculate all amounts

            $("#Balance").append(InvoicesDetail.ReturnObject.Balance);
            $("#Total").append(sum(InvoicesDetail.ReturnObject.Items ));
            $("#SubTotal").append(sum(InvoicesDetail.ReturnObject.Items ));
            console.log(sum(InvoicesDetail.ReturnObject.Items + '.00'));
          }
        ).fail(function(error) {
          notif({
            msg: "Session expired! please login",
            type: "error",
            timeout: 3000
          });
          window.location.href = "../../index.html";
          console.log(error);
          $("#div-forms i").removeClass("fa fa-spinner fa-spin");
        });
      }
    ).fail(function(error) {
      notif({
        msg: "Session expired! please login",
        type: "error",
        timeout: 3000
      });
      $("#div-forms i").removeClass("fa fa-spinner fa-spin");
      window.location.href = "../../index.html";
      console.log(error);
    });
  });

  //Gets the involist list to the ready date table
  $.post(
    "https://api.mydrycleaner.com/q",
    JSON.stringify({
      RequestType: "InvoicesList",
      AccountKey: "NGPERI1344",
      SessionID: sessionID,
      Parameters: {
        FilterTypeID: 129,
        StartDate: "10/22/2018",
        EndDate: output4
      }
    }),
    function(data, status) {
      var InvoicesList = JSON.parse(data);
      var returnObject = InvoicesList.ReturnObject;
      console.log(returnObject);
      sessionStorage.setItem("invoiceID", InvoicesList.ReturnObject.InvoiceID);
      $("#processdate").empty();
      $("#readydate").empty();
      $("#invoice2").empty();
      $("#balance2").empty();
      $("#SoldDateTime").empty();
      $("#invoice3").empty();
      $("#balance3").empty();
      //on success appends the return json to the <td> tags
      //  $.each(returnObject, function(key,val){
      for (var i = 0; i < returnObject.length; i++) {
        var date = new Date(returnObject[i].PromisedDate);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var output =
          date.getFullYear() +
          "/" +
          (month < 10 ? "0" : "") +
          month +
          "/" +
          (day < 10 ? "0" : "") +
          day;
        if (output == null) {
          $("#processdate").append("<tr><td> No orders found</td></tr>");
        }
        //    $("#processdate").append(output);
        //  $("#invoice").append(val.InvoiceKey);
        //  $("#department").append(val.DepartmentGroupName);

        $("#example3").append(
          "<tr><td>" +
            output +
            '</td> <td  id="invoice4"><a href="#"  role="button" data-toggle="modal" data-target="#login-modal">' +
            returnObject[i].InvoiceKey +
            "</a></td> <td>" +
            returnObject[i].DepartmentGroupName +
            " <td> " +
            returnObject[i].Balance +
            " </td><tr>"
        );
        // //on success appends the return json to the second <td> tags
        if (returnObject[i].ReadyDateTime != "0001-01-01T00:00:00") {
          $("#NotFound3").hide();
          var date2 = new Date(returnObject[i].ReadyDateTime);
          var month2 = date2.getMonth() + 1;
          var day2 = date2.getDate();
          var output2 =
            date2.getFullYear() +
            "/" +
            (month2 < 10 ? "0" : "") +
            month2 +
            "/" +
            (day2 < 10 ? "0" : "") +
            day2;

          $("#readydate").append(output2);
          $("#invoice2").append(returnObject[i].InvoiceKey);
          $("#balance2").append(returnObject[i].Balance);
        } else {
          $("#NotFound2").show();
        }
        //on success append to <option id= sold> <td> table
        if (returnObject[i].SoldDateTime != "0001-01-01T00:00:00") {
          var date3 = new Date(returnObject[i].SoldDateTime);
          var month3 = date3.getMonth() + 1;
          var day3 = date3.getDate();
          var output3 =
            date3.getFullYear() +
            "/" +
            (month3 < 10 ? "0" : "") +
            month3 +
            "/" +
            (day < 10 ? "0" : "") +
            day3;
          $("#NotFound").hide();
          $("#SoldDateTime").append(output3);
          $("#invoice3").append(returnObject[i].InvoiceKey);
          $("#balance3").append(returnObject[i].Balance);
        } else {
          $("#NotFound").show();
        }
      }
      //  })
    }
  ).fail(function(error) {
    notif({
      msg: "Error loading order table",
      type: "error",
      timeout: 3000
    });
    console.log(error);
  });
  //on click of invoice column gets  the invoice list
  $(document).on("click", "#invoice4", function() {
    $("#div-forms i").addClass("fa fa-spinner fa-spin");
    var invoicekey = $(this).text();
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output =
      d.getFullYear() +
      "/" +
      (month < 10 ? "0" : "") +
      month +
      "/" +
      (day < 10 ? "0" : "") +
      day;
    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "InvoicesList",
        AccountKey: "NGPERI1344",
        SessionID: sessionID,
        Parameters: {
          FilterTypeID: 129,
          StartDate: "10/22/2018",
          EndDate: output
        }
      }),

      function(data, status) {
        var InvoicesList = JSON.parse(data);
        var returnObject = InvoicesList.ReturnObject;
        //gets the invoiceID for the click invoice of the table
        var returnedData = $.grep(returnObject, function(element, index) {
          return element.InvoiceKey == invoicekey;
        });
        console.log(returnedData[0].InvoiceID);
        //gets the invoice detail by invoiceID
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "InvoiceDetail",
            AccountKey: "NGPERI1344",
            SessionID: sessionID,
            Parameters: {
              InvoiceID: returnedData[0].InvoiceID
            }
          }),
          function(data, status) {
            var InvoicesDetail = JSON.parse(data);
            $("#div-forms i").removeClass("fa fa-spinner fa-spin");
            totalAmountToBePaid = InvoicesDetail.ReturnObject.Total;
            $("#paymentButton").prop("disabled", false);
            console.log(InvoicesDetail.ReturnObject.Total);
            console.log(InvoicesDetail.ReturnObject.Items);
            var date = new Date(InvoicesDetail.ReturnObject.PromisedDate);
            var month = date.getMonth() + 1;
            var day = date.getDate();
            var hour = date.getHours();
            var minute = date.getUTCMinutes();
            var output =
              date.getFullYear() +
              "/" +
              (month < 10 ? "0" : "") +
              month +
              "/" +
              (day < 10 ? "0" : "") +
              day + "  " + hour + ":" + (minute < 10 ? "0" : "") + "0";
            //on success appends to the pop out <p> tags

            $("#DcRegular").append(
              InvoicesDetail.ReturnObject.Items[0].DepartmentName
            );
            $("#keyinvoice").append(InvoicesDetail.ReturnObject.InvoiceKey);

            invoiceKey = InvoicesDetail.ReturnObject.InvoiceKey;
            $("#promiseddate").append("Promised:  " + output);
            $.each(InvoicesDetail.ReturnObject.Items, function(key, val) {
              if (val.Details[0] == null) {
                $("#orderdetails").append(
                  '<p style="text-align:left;">' +
                  val.Quantity + ' - ' + val.CategoryName +
                    "- " +
                    val.ItemName +
                    '<span id="Amount" style="float:right;"><b>' +
                    val.Amount +
                    ' </b></span> </p>   <p id="Name">' +
                    "No color name" +
                    " </p>"
                );
              } else {
                var Name = [];
                val.Details.every(x => {
                  Name.push(x.Name);
                  return x.Name;
              });
                $("#orderdetails").append(
                  '<p style="text-align:left;">' +
                  val.Quantity + ' - ' + val.CategoryName +
                    "- " +
                    val.ItemName +
                    '<span id="Amount" style="float:right;"><b>' +
                    val.Amount +
                    ' </b></span> </p>   <p id="Name">' +
                    Name.join(' , ') +
                    " </p>"
                );
              }
              // $("#orderdetails").append(
              //   '<p style="text-align:left;">' +
              //     val.CategoryName +
              //     "- " +
              //     val.ItemName +
              //     '<span id="Amount" style="float:right;"><b>' +
              //     val.Amount +
              //     ' </b></span> </p>   <p id="Name">' +
              //     val.Details[0].Name +
              //     " </p>"
              // );
            });
            var date;
            if(InvoicesDetail.ReturnObject.Payments.length == 0){
            }
            if(InvoicesDetail.ReturnObject.Payments.length != 0){
              console.log(InvoicesDetail.ReturnObject.Payments);
              date = new Date(InvoicesDetail.ReturnObject.Payments[0].StatusDateTime);
              var month = date.getMonth() + 1;
              var day = date.getDate();
              var hour = date.getHours();
              var minute = date.getUTCMinutes();
              var ampm = (hour >= 12) ? "p.m" : "a.m";
              var output =
                date.getFullYear() +
                "/" +
                (month < 10 ? "0" : "") +
                month +
                "/" +
                (day < 10 ? "0" : "") +
                day + "  " + hour + ":" + minute  + " " + ampm + " -   ";
            }
        
            //calculates the balance of invoice detail amounts
            var Amount = InvoicesDetail.ReturnObject.Items.length;
            function sum(input) {
              var total = 0;
              for (var i = 0; i < Amount; i++) {
                if (isNaN(input[i].Amount)) {
                  continue;
                }
                total += Number(input[i].Amount);
                // console.log(total);
              }
              return total;
            }
            // appends to <b> balance tag after it calculate all amounts
            if(InvoicesDetail.ReturnObject.Payments.length == 0){
              $("#Balance").append(InvoicesDetail.ReturnObject.Balance);
              $("#Total").append(sum(InvoicesDetail.ReturnObject.Items ));
              $("#SubTotal").append(sum(InvoicesDetail.ReturnObject.Items ));
              //#TenderTokenType will now be Discount Name and #AppliedAmount will now be DiscountTotal
              $("#TenderTypeToken").append((InvoicesDetail.ReturnObject.DiscountName !== "" ? discount + InvoicesDetail.ReturnObject.DiscountName : InvoicesDetail.ReturnObject.DiscountName));
              $("#AppliedAmount").append((InvoicesDetail.ReturnObject.DiscountTotal != 0 ? InvoicesDetail.ReturnObject.DiscountTotal : ""));
            }else{
              $("#Balance").append(InvoicesDetail.ReturnObject.Balance);
              $("#Total").append(sum(InvoicesDetail.ReturnObject.Items ));
              $("#SubTotal").append(sum(InvoicesDetail.ReturnObject.Items ));
              $("#TenderTypeToken").append(output + InvoicesDetail.ReturnObject.Payments[0].TenderTypeToken);
              $("#AppliedAmount").append(InvoicesDetail.ReturnObject.Payments[0].AppliedAmount);
            }
         //   console.log(sum(InvoicesDetail.ReturnObject.Items.Amount));
          }
        ).fail(function(error) {
          notif({
            msg: "Session expired! please login",
            type: "error",
            timeout: 3000
          });
          window.location.href = "../../index.html";
          console.log(error);
          $("#div-forms i").removeClass("fa fa-spinner fa-spin");
        });
      }
    ).fail(function(error) {
      notif({
        msg: "Session expired! please login",
        type: "error",
        timeout: 3000
      });
      $("#div-forms i").removeClass("fa fa-spinner fa-spin");
      window.location.href = "../../index.html";
      console.log(error);
    });
  });

  $.post(
    "https://api.mydrycleaner.com/q",
    JSON.stringify({
      RequestType: "CustomerDetail",
      AccountKey: "NGPERI1344",
      SessionID: sessionID
    }),
    function(data, status) {
      var CustomerDetail = JSON.parse(data);

      sessionStorage.setItem(
        "FullName",
        CustomerDetail.ReturnObject.FirstName +
          " " +
          CustomerDetail.ReturnObject.LastName
      );

      sessionStorage.setItem("Email", CustomerDetail.ReturnObject.EmailAddress);
    }
  ).fail(function(error) {
    notif({
      msg: "Session Expired! Please log in",
      type: "error",
      timeout: 3000
    });
    console.log(error);
  });
});
