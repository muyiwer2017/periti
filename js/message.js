$(document).ready(function(){
    // $('.compose').show();
    // $('.backTomessage').hide();
    // $('.composeMessage').hide();
    // $('.viewmessage').show();
    // $('.backTomessage').click( function(){
    //     $('.compose').show();
    //     $('.backTomessage').hide();
    //     $('.viewmessage').show();
    //     $('.composeMessage').hide();
    // });
    // $('.compose').click( function(){
    //     $('.compose').hide();
    //     $('.backTomessage').show();
    //     $('.viewmessage').hide();
    //     $('.composeMessage').show();
    // });
    // $('.sent').click( function(){
    //     $('.compose').show();
    //     $('.backTomessage').hide();
    //     $('.viewmessage').show();
    //     $('.composeMessage').hide();
    // });
    // $('.newMessage').click( function(){
    //     $('.compose').hide();
    //     $('.backTomessage').show();
    //     $('.viewmessage').hide();
    //     $('.composeMessage').show();
    // });

    var sessionID =  sessionStorage.getItem("userSessionID", sessionID);
    var Email =  sessionStorage.getItem("Email",Email);
    $(".composeButton").click(function(){
        $("loading").prop("disabled", true);
        $(".loading").addClass("fa fa-spinner fa-spin");
        var subject = $(".subject").val();
        var textAreaMessage = $(".textAreaMessage").val();
        $.post(
            "https://api.mydrycleaner.com/q",
            JSON.stringify({
              RequestType: "MessageToManagerNoUser",
              AccountKey: "NGPERI1344",
              SessionID: sessionID,
              Parameters:{
                Subject: subject,
                Message: textAreaMessage,
                FromEmail: Email
              }
            }),
            function(data, status) {
                var managerMessageList = JSON.parse(data);
                var returnObject = managerMessageList.ReturnObject;
                console.log(returnObject);
                $("loading").prop("disabled", false);
                $(".loading").removeClass("fa fa-spinner fa-spin");
                $(".textAreaMessage").val('');
                $(".subject").val('');
                notif({
                    msg: "message sent to manager successfully",
                    type: "success",
                    timeout: 3000
                  });
            }
        ).fail(function(error) {
            $("loading").prop("disabled", false);
                $(".loading").addClass("fa fa fa-envelope-o");
            notif({
              msg: "Session expired! please login",
              type: "error",
              timeout: 3000
            });
            window.location.href = "../../index.html";
            console.log(error)
          })
    });

});