$(document).ready(function() {
  var sessionID;

  $("#signUpForm").submit(function(event) {
    //get Form Values
    var signUpFormEmail = $("#signUpFormEmail").val();
    var signUpFormPhoneNumber = $("#signUpFormPhoneNumber").val();
    var signUpFormFirstName = $("#signUpFormFirstName").val();
    var signUpFormLastName = $("#signUpFormLastName").val();
    var signUpFormPassword = $("#signUpFormPassword").val();
    var signUpFormConfirmPassword = $("#signUpFormConfirmPassword").val();
    var AccountNodeID = "ebf228d9-c485-42b1-9148-35b6d750c2c3";

    $(".submitButton i").addClass("fa fa-spinner fa-spin");

    //get token
    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "GetToken",
        AccountKey: "NGPERI1344",
        SecurityID: "8C41892F-24BE-440B-86D3-939394409157"
      }),
      function(data, status) {
        var tokenData = JSON.parse(data);
        sessionID = tokenData.ReturnObject.SessionID;
        sessionStorage.setItem("sessionID", sessionID);
      }
    )
      .fail(function(error) {
        notif({
          msg: "There seems to be an error",
          type: "error"
        });
      })
      //Sign Up form
      .done(function(data) {
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "Signup",
            AccountKey: "NGPERI1344",
            SessionID: sessionID,
            Parameters: {
              AcceptTermsBypass: true,
              AccountNodeID: AccountNodeID,
              Firstname: signUpFormFirstName,
              Lastname: signUpFormLastName,
              EmailAddress: signUpFormEmail,
              Password: signUpFormPassword,
              ClientAccountID: "",
              ServiceType: "RETAIL"
            }
          }),
          function(data, status) {
            notif({
              msg: "Signup successful",
              type: "success",
              timeout: 3000
            });
            var returnData = JSON.parse(data);
          }
        ).fail(function(error) {
          notif({
            msg: "There seems to be an error2",
            type: "error"
          });
        });
      })
      .always(function() {
        $(".submitButton i").removeClass("fa fa-spinner fa-spin");
      });

    event.preventDefault();
  });
});
