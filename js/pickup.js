$(document).ready(function() {

    var sessionID =  sessionStorage.getItem("userSessionID", sessionID);
    var PickupDate;
    $("#datepicker").on("change", function() {
        PickupDate = $(this).val();
    });
    var DeliveryDate;
    $("#datepicker2").on("change", function() {
        DeliveryDate = $(this).val();
    });
    //on click of submit pickup button
    $(".submitPickup").click(function(){
        $("loading").prop("disabled", true);
        $(".loading").addClass("fa fa-spinner fa-spin");
        var PickupComment = $("#PickupComment").val();
        var cleaningInstructions = $("#cleaningInstructions").val();
        $.post(
            "https://api.mydrycleaner.com/q",
            JSON.stringify({
              RequestType: "PickupRequest",
              AccountKey: "NGPERI1344",
              SessionID: sessionID,
              Parameters:{
                PickupDate: PickupDate,
                DeliveryDate: DeliveryDate,
                Comments: PickupComment,
                InstructionsRequests: cleaningInstructions,
                VisitType: "Pickup",
                TermsAndConditions: true
              }
            }),
            function(data, status) {
                var pickupMessage = JSON.parse(data);
                if(pickupMessage.Failed == true){
                    console.log(pickupMessage);
                    $("loading").prop("disabled", false);
                    $(".loading").removeClass("fa fa-spinner fa-spin");
                    notif({
                        msg: pickupMessage.Message,
                        type: "error",
                        timeout: 3000
                      });
                }else{
                    $("loading").prop("disabled", false);
                    $(".loading").removeClass("fa fa-spinner fa-spin");
                    notif({
                        msg: pickupMessage.Message,
                        type: "success",
                        timeout: 3000
                      });
                }
               
            }
        ).fail(function(error) {
            $("loading").prop("disabled", false);
                $(".loading").addClass("fa fa fa-envelope-o");
            notif({
              msg: "Session expired! please login",
              type: "error",
              timeout: 3000
            });
            console.log(error)
          })
    });
});