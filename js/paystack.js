function payWithPaystack() {
  email = sessionStorage.getItem("Email") || "";
  fullName = sessionStorage.getItem("FullName") || "";
  var handler = PaystackPop.setup({
    // key: "pk_test_7925b9f08acfadee13c4370abbade52603225111",
    key: "pk_live_2f9bf2b11f82915a5a0dfaa1747027ca38442be9",
    email: email,
    amount: totalAmountToBePaid * 100,
    metadata: {
      custom_fields: [
        {
          display_name: "Name",
          variable_name: "Name",
          value: fullName
        },
        {
          display_name: "Invoice Number",
          variable_name: "Invoice_Number",
          value: invoiceKey
        }
      ]
    },
    callback: function(response) {
      console.log(response);
      alert("success. transaction ref is " + response.reference);
      sessionStorage.setItem("transactionReference", response.reference);
    },
    onClose: function() {
      alert("window closed");
    }
  });
  handler.openIframe();
}
