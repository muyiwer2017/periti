$(document).ready(function() {
  var sessionID = sessionStorage.getItem("userSessionID", sessionID);
  // $('.starch').click(function () {
  //   console.log("starch");
  //   $('.starch').html('<option>None</option><option>Light</option><option>Medium</option><option>Heavy</option>')
  // })
  // $('.finish').click(function () {
  //   $('.finish').html('<option>Hung</option><option>Fold</option>')
  // })
  var Cdetails;
  var PickupDate;
  $("#datepicker").on("change", function() {
    PickupDate = $(this).val();
  });
  // var starshValue;
  //= $('.starch').val();
  // var finishValue;
  //= $('.finish').val();

  $.post(
    "https://api.mydrycleaner.com/q",
    JSON.stringify({
      RequestType: "CustomerDetail",
      AccountKey: "NGPERI1344",
      SessionID: sessionID
    }),
    function(data, status) {
      var CustomerDetail = JSON.parse(data);
      Cdetails = JSON.parse(data);
      console.log(CustomerDetail.ReturnObject);
      //convert returned date
      var date3 = new Date(CustomerDetail.ReturnObject.Birthdate);
      var month3 = date3.getMonth() + 1;
      var day3 = date3.getDate();
      var output3 =
        date3.getFullYear() +
        "/" +
        (month3 < 10 ? "0" : "") +
        month3 +
        "/" +
        (day3 < 10 ? "0" : "") +
        day3;
      console.log(CustomerDetail.ReturnObject.EmailAddress);
      console.log(CustomerDetail.ReturnObject.PrimaryAddress.State);
      $("#Email").val(CustomerDetail.ReturnObject.EmailAddress);
      console.log($("#Email").val());
      $("#FirstName").val(CustomerDetail.ReturnObject.LastName);
      $("#LastName").val(CustomerDetail.ReturnObject.FirstName);

      sessionStorage.setItem(
        "FullName",
        CustomerDetail.ReturnObject.FirstName +
          " " +
          CustomerDetail.ReturnObject.LastName
      );

      $(".Birthdate").val(output3);

      $("#phone").val(CustomerDetail.ReturnObject.Phones[0].Number);
      //primary address
      $("#address1").val(CustomerDetail.ReturnObject.PrimaryAddress.Address1);
      $("#address2").val(CustomerDetail.ReturnObject.PrimaryAddress.Address2);
      $("#city").val(CustomerDetail.ReturnObject.PrimaryAddress.City);
      $("#state").append(CustomerDetail.ReturnObject.PrimaryAddress.State);
      $("#postalCode").val(CustomerDetail.ReturnObject.PrimaryAddress.Zip);
      //delivery
      $("#deliveryaddress1").val(
        CustomerDetail.ReturnObject.DeliveryAddress.Address1
      );
      $("#deliveryaddress2").val(
        CustomerDetail.ReturnObject.DeliveryAddress.Address2
      );
      $("#deliverycity").val(CustomerDetail.ReturnObject.DeliveryAddress.City);
      $("#deliverystate").append(
        CustomerDetail.ReturnObject.DeliveryAddress.State
      );
      $("#deliverypostalCode").val(
        CustomerDetail.ReturnObject.DeliveryAddress.Zip
      );
      //billings
      $("#billingaddress1").val(
        CustomerDetail.ReturnObject.BillingAddress.Address1
      );
      $("#billingaddress2").val(
        CustomerDetail.ReturnObject.BillingAddress.Address2
      );
      $("#billingcity").val(CustomerDetail.ReturnObject.BillingAddress.City);
      $("#billingstate").append(
        CustomerDetail.ReturnObject.BillingAddress.State
      );
      $("#billingpostalCode").val(
        CustomerDetail.ReturnObject.BillingAddress.Zip
      );
      //preference
      console.log(CustomerDetail.ReturnObject.Preferences[1].Value);
      console.log(CustomerDetail.ReturnObject.Preferences[0].Value);
      //  var starshValue = CustomerDetail.ReturnObject.Preferences[0].Value;
      // var finishValue = CustomerDetail.ReturnObject.Preferences[1].Value;
      $(".starch").val(CustomerDetail.ReturnObject.Preferences[0].Value);
      $(".finish").val(CustomerDetail.ReturnObject.Preferences[1].Value);
      // cleaning instruction
      $("#cleaninginstruction").append(
        CustomerDetail.ReturnObject.CleaningInstructions
      );
    }
  ).fail(function(error) {
    notif({
      msg: "Session Expired! Please log in",
      type: "error",
      timeout: 3000
    });
    console.log(error);
  });

  // on click of starsh field
  $(".starch").click(function() {
    var starshValue = $(this).val();
    console.log(starshValue);
    if (starshValue == "None") {
      $("#starch").hide();
      $("#None").hide();
      $("#Light").show();
      $("#Medium").show();
      $("#Heavy").show();
    }
    if (starshValue == "Light") {
      $("#starch").hide();
      $("#Light").hide();
      $("#None").show();
      $("#Medium").show();
      $("#Heavy").show();
    }
    if (starshValue == "Medium") {
      $("#starch").hide();
      $("#Medium").hide();
      $("#None").show();
      $("#Heavy").show();
      $("#Light").show();
    }
    if (starshValue == "Heavy") {
      $("#starch").hide();
      $("#Heavy").hide();
      $("#Medium").show();
      $("#None").show();
      $("#Light").show();
    }
  });

  // on click of finish field
  $(".finish").click(function() {
    var finishValue = $(this).val();
    console.log(finishValue);
    if (finishValue == "Hung") {
      $("#finish").hide();
      $("#Hung").hide();
      $("#Fold").show();
    }
    if (finishValue == "Fold") {
      $("#finish").hide();
      $("#Fold").hide();
      $("#Hung").show();
    }
  });

  //on click on save button
  $(".saveCustomerDetails").click(function() {
    $(".saveCustomerDetails").prop("disabled", true);
    $(".saveCustomerDetails i").addClass("fa fa-spinner fa-spin");
    var updatedDetails = {
      Birthdate: PickupDate,
      EmailAddress: $("#Email").val(),
      FirstName: $("#FirstName").val(),
      LastName: $("#LastName").val(),
      CleaningInstructions: $("#cleaninginstruction").val(),
      Phones: [
        {
          Number: $("#phone").val()
        }
      ],
      PrimaryAddress: {
        Address1: $("#address1").val(),
        Address2: $("#address2").val(),
        City: $("#city").val(),
        State: $("#state").val(),
        Zip: $("#postalCode").val()
      },
      BillingAddress: {
        Address1: $("#billingaddress1").val(),
        Address2: $("#billingaddress2").val(),
        City: $("#billingcity").val(),
        State: $("#billingstate").val(),
        Zip: $("#billingpostalCode").val()
      },
      DeliveryAddress: {
        Address1: $("#deliveryaddress1").val(),
        Address2: $("#deliveryaddress2").val(),
        City: $("#deliverycity").val(),
        State: $("#deliverystate").val(),
        Zip: $("#deliverypostalCode").val()
      },
      Preferences: [
        {
          Name: "Starch",
          Value: $(".starch").val()
        },
        {
          Name: "Finish",
          Value: $(".finish").val()
        }
      ]
    };
    var returnObject = Cdetails.ReturnObject;
    var replaceReturnObject = $.extend(returnObject, updatedDetails);
    console.log(replaceReturnObject);
    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "SaveCustomer",
        AccountKey: "NGPERI1344",
        SessionID: sessionID,
        Parameters: replaceReturnObject
      }),
      function(data, status) {
        $(".saveCustomerDetails i").removeClass("fa fa-spinner fa-spin");
        $(".saveCustomerDetails").prop("disabled", false);
        var CustomerDetail = JSON.parse(data);
        console.log(CustomerDetail);
        notif({
          msg: CustomerDetail.Message,
          type: "success",
          timeout: 3000
        });
        //Get customer detail after updating
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "CustomerDetail",
            AccountKey: "NGPERI1344",
            SessionID: sessionID
          }),
          function(data, status) {
            var CustomerDetail = JSON.parse(data);
            console.log(CustomerDetail.ReturnObject);
            console.log(CustomerDetail.ReturnObject.EmailAddress);
            console.log(CustomerDetail.ReturnObject.PrimaryAddress.State);
            $("#Email").val(CustomerDetail.ReturnObject.EmailAddress);
            console.log($("#Email").val());

            $("#FirstName").val(CustomerDetail.ReturnObject.LastName);
            $("#LastName").val(CustomerDetail.ReturnObject.FirstName);
            $("#phone").val(CustomerDetail.ReturnObject.Phones[0].Number);
            //primary address
            $("#address1").val(
              CustomerDetail.ReturnObject.PrimaryAddress.Address1
            );
            $("#address2").val(
              CustomerDetail.ReturnObject.PrimaryAddress.Address2
            );
            $("#city").val(CustomerDetail.ReturnObject.PrimaryAddress.City);
            $("#state").append(
              CustomerDetail.ReturnObject.PrimaryAddress.State
            );
            $("#postalCode").append(
              CustomerDetail.ReturnObject.PrimaryAddress.PostalCodeMask
            );
            //delivery
            $("#deliveryaddress1").val(
              CustomerDetail.ReturnObject.DeliveryAddress.Address1
            );
            $("#deliveryaddress2").val(
              CustomerDetail.ReturnObject.DeliveryAddress.Address2
            );
            $("#deliverycity").val(
              CustomerDetail.ReturnObject.DeliveryAddress.City
            );
            $("#deliverystate").append(
              CustomerDetail.ReturnObject.DeliveryAddress.State
            );
            $("#deliverypostalCode").append(
              CustomerDetail.ReturnObject.DeliveryAddress.PostalCodeMask
            );
            //billings
            $("#billingaddress1").val(
              CustomerDetail.ReturnObject.BillingAddress.Address1
            );
            $("#billingaddress2").val(
              CustomerDetail.ReturnObject.BillingAddress.Address2
            );
            $("#billingcity").val(
              CustomerDetail.ReturnObject.BillingAddress.City
            );
            $("#billingstate").append(
              CustomerDetail.ReturnObject.BillingAddress.State
            );
            $("#billingpostalCode").append(
              CustomerDetail.ReturnObject.BillingAddress.PostalCodeMask
            );
            //preference
            $("#starch").html(CustomerDetail.ReturnObject.Preferences[0].Value);
            $("#finish").html(CustomerDetail.ReturnObject.Preferences[1].Value);
            // cleaning instruction
            $("#cleaninginstruction").html(
              CustomerDetail.ReturnObject.CleaningInstructions
            );
          }
        ).fail(function(error) {
          notif({
            msg: "There seems to be an error",
            type: "error",
            timeout: 3000
          });
          console.log(error);
        });
      }
    ).fail(function(error) {
      $(".saveCustomerDetails i").removeClass("fa fa-spinner fa-spin");
      $(".saveCustomerDetails").prop("disabled", false);
      notif({
        msg: "There seems to be an error",
        type: "error",
        timeout: 3000
      });
      console.log(error);
    });
  });
});
