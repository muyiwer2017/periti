$(document).ready(function() {
  var sessionID;

  $.validator.setDefaults({
    highlight: function(element) {
      $(element)
        .closest(".form-group")
        .addClass("has-error");
    },
    unhighlight: function(element) {
      $(element)
        .closest(".form-group")
        .removeClass("has-error");
    },
    errorElement: "span",
    errorClass: "help-block",
    errorPlacement: function(error, element) {
      if (element.parent(".input-group").length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  $(".forgotpasswordButton").click(function() {
    $("#PasswordResetForm").validate({
      rules: {
        email: {
          required: true
        }
      },
      messages: {
        email: {
          required: "Please provide an email"
        }
      },
      submitHandler: function() {
        $("#myModal").modal("show");
        $(".modal-body").html(
          '<h4 style="text-align: center">Loading....</h4>'
        );
        var Email = $("#email").val();
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "GetToken",
            AccountKey: "NGPERI1344",
            SecurityID: "8C41892F-24BE-440B-86D3-939394409157"
          }),
          function(data, status) {
            var tokenData = JSON.parse(data);
            sessionID = tokenData.ReturnObject.SessionID;
            sessionStorage.setItem("sessionID", sessionID);
            sessionStorage.setItem("Email", Email);
          }
        )
          .fail(function(error) {
            notif({
              msg: "There seems to be an error",
              type: "error",
              timeout: 3000
            });
          })
          //forgot password endpoint
          .done(function(data) {
            $.post(
              "https://api.mydrycleaner.com/q",
              JSON.stringify({
                RequestType: "RememberPasswordRequest",
                AccountKey: "NGPERI1344",
                SessionID: sessionID,
                Parameters: {
                  EmailAddress: Email,
                  IPAddress: "",
                  SendEmail: true,
                  ResetFinishURL:
                    "http://www.theperiti.com/PeritiApp/password-reset.html?token=%code%&Email=" +
                    Email
                }
              }),
              function(data, status) {
                var returnData = JSON.parse(data);
                console.log(returnData);
                if (returnData.Failed == true) {
                  notif({
                    msg: returnData.Message,
                    type: "error"
                  });
                  $(".modal-body").html(
                    '<i class="fa fa-warning" style="font-size:48px;color:red;padding:0px 0px 0px 200px;"></i><span style="font-size:20px;color:green;"> Email not sent</span><p class="lead"> Email inputed on login field is empty or invalid</p><hr>'
                  );
                } else {
                  //
                  $(".modal-body").html(
                    '<i class="fa fa-check-circle" style="font-size:48px;color:green;padding:0px 0px 0px 200px;"></i><span style="font-size:20px;color:green;"> Email sent</span><p class="lead"><strong>Please check your email</strong> for further instructions on how to reset password.</p>'
                  );
                }
              }
            ).fail(function(error) {
              notif({
                msg: "There seems to be an error",
                type: "error"
              });
            });
          });
      }
    });
  });
});
