$(document).ready(function(){
    var sessionID =  sessionStorage.getItem("userSessionID", sessionID);
    var startDate;
    var endDate;
    $("#datepicker").on("change",function(){
        startDate = $(this).val();
      });
      $("#datepicker2").on("change",function(){
        endDate = $(this).val();
      });
      var comment = $(".comments").val();
      console.log(startDate + " " +endDate + " "  + comment + " ")
   
        $(".suspendService").click(function(){
          console.log(startDate + " " +endDate + " "  + comment + " ")
          $(".submitButton").prop("disabled", true);
          $(".submitButton i").addClass("fa fa-spinner fa-spin");
              //onclick excutes suspend service endpoint
              $.post(
                "https://api.mydrycleaner.com/q",
                JSON.stringify({
                  RequestType: "CancellationRequest",
                  AccountKey: "NGPERI1344",
                  SessionID: sessionID,
                  Parameters: {
                    FromDate: startDate,
                    ToDate: endDate,
                    Comments: comment,
                    InstructionRequests: "null"
                  }
                }),
                function(data, status) {
                    var SuspendServiceLIst = JSON.parse(data);
                    var returnObject = SuspendServiceLIst.ReturnObject;
                    console.log(SuspendServiceLIst);
                    console.log(returnObject);
                    if(SuspendServiceLIst.Failed == true){
                      $(".submitButton i").removeClass("fa fa-spinner fa-spin");
                      $(".submitButton").prop("disabled", false);
                      notif({
                          msg: SuspendServiceLIst.Message,
                          type: "error",
                          timeout: 3000
                        });
                    }else{
                      $(".submitButton i").removeClass("fa fa-spinner fa-spin");
                      $(".submitButton").prop("disabled", false);
                      notif({
                          msg: "Service suspended successfully.",
                          type: "success",
                          timeout: 3000
                        });
                    }
                   
                }
            ).fail(function(error) {
              $(".submitButton i").removeClass("fa fa-spinner fa-spin");
              $(".submitButton").prop("disabled", false);
              notif({
                msg: "Not successful! please try again",
                type: "error",
                timeout: 3000
              });
              console.log(error)
            })
        });

        //on load of the pending suspension pop gets the list on pending suspension
        $(".suspenseButton").click(function(){
    $.post(
        "https://api.mydrycleaner.com/q",
        JSON.stringify({
          RequestType: "PendingCancellations",
          AccountKey: "NGPERI1344",
          SessionID: sessionID
        }),
        function(data, status) {
            var PickupsList = JSON.parse(data);
            var returnObject = PickupsList.ReturnObject;
            console.log(returnObject);
            //on success appends the return json to the <td> tags
            if($("#suspendDate").val() == null){
           for(var i = 0; i < returnObject.length; i++ )
           {
                //on success append to <option id= sold> <td> table
                    if(returnObject!= null){
                        // date format for start date
                      var date3 =  new Date(returnObject[i].FromDate);
                      var month3 = date3.getMonth()+1;
                      var day3 = date3.getDate();
                      var output3 = date3.getFullYear() + '/' +
                          (month3<10 ? '0' : '') + month3 + '/' +
                          (day3<10 ? '0' : '') + day3;
                        //date format for end date
                          var date =  new Date(returnObject[i].ToDate);
                          var month = date.getMonth()+1;
                          var day1 = date.getDate();
                          var output = date3.getFullYear() + '/' +
                              (month<10 ? '0' : '') + month + '/' +
                              (day1<10 ? '0' : '') + day1;
                          $("#NotFound").hide();
                          $("#example1").append('<tr><td id="suspendDate">'+ output3 +'</td> <td>'
                          + output + '</td> <td> '+ returnObject[i].Comments + ' </td><tr>');
                       //   $("#startDate").append(output3);
                        //  $("#endDate").append(output);
                         // $("#Comments").append(returnObject[i].Comments);
                         console.log( returnObject[i].Comments);
                    }else{
                      $("#NotFound").show();
                    }
                  }
                }
          //  })
        }
    ).fail(function(error) {
      notif({
        msg: "Session expired! please login",
        type: "error",
        timeout: 3000
      });
      window.location.href = "../../index.html";
      console.log(error)
    })
  });
})