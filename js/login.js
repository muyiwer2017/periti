$(document).ready(function() {
  $.validator.setDefaults({
    highlight: function(element) {
      $(element)
        .closest(".form-group")
        .addClass("has-error");
    },
    unhighlight: function(element) {
      $(element)
        .closest(".form-group")
        .removeClass("has-error");
    },
    errorElement: "span",
    errorClass: "help-block",
    errorPlacement: function(error, element) {
      if (element.parent(".input-group").length) {
        error.insertAfter(element.parent());
      } else {
        error.insertAfter(element);
      }
    }
  });

  var sessionID;
  var deliveryType;

  $(".register").hide();
  $(".pickup").hide();
  $(".Login").show();
  $(".Address").hide();
  $(".confirm").hide();

  $(".login2").click(function() {
    $(".register").hide();
    $(".Login").show();
    $(".pickup").hide();
    $(".Address").hide();
    $(".confirm").hide();
  });

  $(".register2").click(function() {
    $(".Login").hide();
    $(".register").show();
    $(".pickup").hide();
    $(".Address").hide();
  });

  $(".next").click(function() {
    $("#registerForm").validate({
      rules: {
        registerPassword: {
          required: true
        },
        registerConfirmPassword: {
          required: true,
          equalTo: "#registerPassword"
        },
        registerPhoneNumber: {
          required: true,
          minlength: 11,
          maxlength: 11
        }
      },
      messages: {
        registerPassword: {
          required: "Please provide a password"
        },
        registerConfirmPassword: {
          required: "Please provide a password",
          equalTo: "Please enter the same password as above"
        },
        registerPhoneNumber: {
          required: "Please provide a phone number",
          minlength: "Incomplete number of characters",
          maxlength: "Too many characters"
        }
      },
      submitHandler: function() {
        $(".pickup").show();
        $(".Login").hide();
        $(".register").hide();
        $(".Address").hide();
        $(".confirm").hide();
      }
    });
  });

  $(".stores").click(function() {
    deliveryType = "RETAIL";
    $(".pickup").hide();
    $(".Login").hide();
    $(".register").hide();
    $(".Address").hide();
    $(".confirm").show();
  });

  $(".delivery").click(function() {
    deliveryType = "DELIVERY";
    $(".pickup").hide();
    $(".Login").hide();
    $(".register").hide();
    $(".Address").show();
    $(".confirm").hide();
  });

  $(".pickupback").click(function() {
    $(".pickup").hide();
    $(".Login").hide();
    $(".register").show();
    $(".Address").hide();
    $(".confirm").hide();
  });

  $(".backaddress").click(function() {
    $(".pickup").show();
    $(".Login").hide();
    $(".register").hide();
    $(".Address").hide();
    $(".confirm").hide();
  });

  $(".nextaddress").click(function() {
    $("#addressForm").validate({
      submitHandler: function() {
        $(".pickup").hide();
        $(".Login").hide();
        $(".register").hide();
        $(".Address").hide();
        $(".confirm").show();
      }
    });
  });

  $(".backconfirm").click(function() {
    $(".pickup").show();
    $(".Login").hide();
    $(".register").hide();
    $(".Address").hide();
    $(".confirm").hide();
  });

  $(".confirmsave").click(function() {
    var registerEmail = $("#registerEmail").val();
    var registerFirstName = $("#registerFirstName").val();
    var registerLastName = $("#registerLastName").val();
    var registerPassword = $("#registerPassword").val();
    var registerPhoneNumber = $("#registerPhoneNumber").val();
    var registerHearAboutUs = $("#registerHearAboutUs").val();
    var registerPromoCode = $("#registerPromoCode").val();
    var addressName1 = $("#addressName1").val();
    var addressName2 = $("#addressName2").val();
    var addressCity = $("#addressCity").val();
    var addressState = $("#addressState").val();
    var addressPostalCode = $("#addressPostalCode").val();
    var AccountNodeID = "ebf228d9-c485-42b1-9148-35b6d750c2c3";

    $(".confirmsave").prop("disabled", true);

    $(".confirmsave i").addClass("fa fa-spinner fa-spin");

    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "GetToken",
        AccountKey: "NGPERI1344",
        SecurityID: "8C41892F-24BE-440B-86D3-939394409157"
      }),
      function(data, status) {
        var tokenData = JSON.parse(data);
        sessionID = tokenData.ReturnObject.SessionID;
        sessionStorage.setItem("sessionID", sessionID);
      }
    )
      .fail(function(error) {
        notif({
          msg: "There seems to be an error",
          type: "error",
          timeout: 3000
        });
        $(".confirmsave").prop("disabled", false);
        $(".confirmsave i").removeClass("fa fa-spinner fa-spin");
      })
      //signup
      .done(function(data) {
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "Signup",
            AccountKey: "NGPERI1344",
            SessionID: sessionID,
            Parameters: {
              AcceptTerms: true,
              Phones: [
                {
                  Extension: "",
                  Number: registerPhoneNumber,
                  PhoneType: "Mobile"
                }
              ],
              AccountNodeID: AccountNodeID,
              Firstname: registerFirstName,
              Lastname: registerLastName,
              EmailAddress: registerEmail,
              Password: registerPassword,
              RewardsProgramName: registerPromoCode,
              ReferralSource: registerHearAboutUs,
              ClientAccountID: "",
              ServiceType: deliveryType,
              DeliveryAddress: {
                Address1: addressName1,
                Address2: addressName2,
                City: addressCity,
                State: addressState,
                Zip: addressPostalCode
              }
            }
          }),
          function(data, status) {
            notif({
              msg: "Signup successful",
              type: "success",
              timeout: 3000
            });
            var returnData = JSON.parse(data);
            console.log(returnData);
            if (returnData.Failed == true) {
              notif({
                msg: returnData.Message,
                type: "error"
              });
            } else {
              notif({
                msg: "signup successful",
                type: "success",
                timeout: 3000
              });

              console.log(registerEmail);
              console.log(registerPassword);

              $.post(
                "https://api.mydrycleaner.com/q",
                JSON.stringify({
                  RequestType: "GetToken",
                  AccountKey: "NGPERI1344",
                  SecurityID: "8C41892F-24BE-440B-86D3-939394409157"
                }),
                function(data, status) {
                  var tokenData = JSON.parse(data);
                  sessionID = tokenData.ReturnObject.SessionID;
                  sessionStorage.setItem("sessionID", sessionID);
                  sessionStorage.setItem("Email", registerEmail);
                }
              )
                .fail(function(error) {
                  notif({
                    msg: "There seems to be an error",
                    type: "error",
                    timeout: 3000
                  });
                  // $(".submitButton i").removeClass("fa fa-spinner fa-spin"); x
                  // $(".submitButton").prop("disabled", false); x
                })
                //login
                .done(function(data) {
                  $.post(
                    "https://api.mydrycleaner.com/q",
                    JSON.stringify({
                      RequestType: "Login",
                      AccountKey: "NGPERI1344",
                      SessionID: sessionID,
                      Parameters: {
                        User: registerEmail,
                        Password: registerPassword
                      }
                    }),
                    function(data, status) {
                      var returnData = JSON.parse(data);
                      console.log(returnData);
                      if (returnData.Failed == true) {
                        $(".submitButton i").removeClass(
                          "fa fa-spinner fa-spin"
                        );
                        $(".submitButton").prop("disabled", false);
                        notif({
                          msg: returnData.Message,
                          type: "error"
                        });
                      } else {
                        var userSessionID = returnData.ReturnObject.SessionID;
                        sessionStorage.setItem("userSessionID", userSessionID);

                        notif({
                          msg: "Login successful",
                          type: "success",
                          timeout: 3000
                        });
                        window.location.href = "./pages/forms/pickup.html";
                      }
                    }
                  ).fail(function(error) {
                    notif({
                      msg: "There seems to be an error",
                      type: "error"
                    });
                  });
                });
            }
            $(".confirmsave").prop("disabled", false);
            $(".confirmsave i").removeClass("fa fa-spinner fa-spin");
          }
        ).fail(function(error) {
          notif({
            msg: "There seems to be an error",
            type: "error"
          });
          $(".confirmsave").prop("disabled", false);
          $(".confirmsave i").removeClass("fa fa-spinner fa-spin");
        });
      });
    event.preventDefault();
  });

  //get token
  $(".submitButton").click(function(event) {
    var loginFormEmail = $("#loginFormEmail").val();
    var loginFormPassword = $("#loginFormPassword").val();
    $(".submitButton").prop("disabled", true);

    $(".submitButton i").addClass("fa fa-spinner fa-spin");

    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "GetToken",
        AccountKey: "NGPERI1344",
        SecurityID: "8C41892F-24BE-440B-86D3-939394409157"
      }),
      function(data, status) {
        var tokenData = JSON.parse(data);
        sessionID = tokenData.ReturnObject.SessionID;
        sessionStorage.setItem("sessionID", sessionID);
        sessionStorage.setItem("Email", loginFormEmail);
      }
    )
      .fail(function(error) {
        notif({
          msg: "There seems to be an error",
          type: "error",
          timeout: 3000
        });
        $(".submitButton i").removeClass("fa fa-spinner fa-spin");
        $(".submitButton").prop("disabled", false);
      })
      //login
      .done(function(data) {
        $.post(
          "https://api.mydrycleaner.com/q",
          JSON.stringify({
            RequestType: "Login",
            AccountKey: "NGPERI1344",
            SessionID: sessionID,
            Parameters: {
              User: loginFormEmail,
              Password: loginFormPassword
            }
          }),
          function(data, status) {
            var returnData = JSON.parse(data);
            console.log(returnData);
            if (returnData.Failed == true) {
              $(".submitButton i").removeClass("fa fa-spinner fa-spin");
              $(".submitButton").prop("disabled", false);
              notif({
                msg: returnData.Message,
                type: "error"
              });
            } else {
              var userSessionID = returnData.ReturnObject.SessionID;
              sessionStorage.setItem("userSessionID", userSessionID);

              notif({
                msg: "Login successful",
                type: "success",
                timeout: 3000
              });
              window.location.href = "./pages/forms/pickup.html";

              $(".submitButton i").removeClass("fa fa-spinner fa-spin");
              $(".submitButton").prop("disabled", false);
            }
          }
        ).fail(function(error) {
          notif({
            msg: "There seems to be an error",
            type: "error"
          });
          $(".submitButton i").removeClass("fa fa-spinner fa-spin");
          $(".submitButton").prop("disabled", false);
        });
      });

    event.preventDefault();
  });
});
