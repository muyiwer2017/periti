$(document).ready(function() {
  var sessionID;
  // Read a page's GET URL variables and return them as an associative array.
  // function getUrlVars()
  // {

  //     return vars;

  // }
  // get token
  $.post(
    "https://api.mydrycleaner.com/q",
    JSON.stringify({
      RequestType: "GetToken",
      AccountKey: "NGPERI1344",
      SecurityID: "8C41892F-24BE-440B-86D3-939394409157"
    }),
    function(data, status) {
      var tokenData = JSON.parse(data);
      sessionID = tokenData.ReturnObject.SessionID;
      sessionStorage.setItem("sessionID", sessionID);
    }
  ).fail(function(error) {
    notif({
      msg: "There seems to be an error",
      type: "error",
      timeout: 3000
    });
    $(".resetPasswordButton i").removeClass("fa fa-spinner fa-spin");
    $(".resetPasswordButton").prop("disabled", false);
  });

  //Reset password endpoint
  $(".resetPasswordButton").click(function(event) {
    $(".resetPasswordButton").prop("disabled", true);
    $(".resetPasswordButton i").addClass("fa fa-spinner fa-spin");
    var NewPassword = $("#Password").val();

    var vars = [],
      hash;
    var hashes = window.location.href
      .slice(window.location.href.indexOf("?") + 1)
      .split("&");
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split("=");
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    console.log(vars);
    var PasswordToken = vars["token"];
    var Email = vars["Email"];
    $.post(
      "https://api.mydrycleaner.com/q",
      JSON.stringify({
        RequestType: "RememberPasswordFinish",
        AccountKey: "NGPERI1344",
        SessionID: sessionID,
        Parameters: {
          EmailAddress: Email,
          RememberKey: PasswordToken,
          NewPassword: NewPassword
        }
      }),
      function(data, status) {
        var returnData = JSON.parse(data);
        console.log(returnData);
        if (returnData.Failed == true) {
          $(".resetPasswordButton i").removeClass("fa fa-spinner fa-spin");
          $(".resetPasswordButton").prop("disabled", false);
          if (returnData.Message == "Password strength insufficient") {
            notif({
              msg:
                "<p>Use at least one capital letter</p><p>and one numeral or special symbol</p>",
              type: "error"
            });
          } else {
            notif({
              msg: returnData.Message,
              type: "error"
            });
          }
        } else {
          //
          window.location.href = "./index.html";
          $(".resetPasswordButton i").removeClass("fa fa-spinner fa-spin");
          $(".resetPasswordButton").prop("disabled", false);
        }
      }
    ).fail(function(error) {
      notif({
        msg: "There seems to be an error",
        type: "error"
      });
      $(".resetPasswordButton i").removeClass("fa fa-spinner fa-spin");
      $(".resetPasswordButton").prop("disabled", false);
    });
  });
});
